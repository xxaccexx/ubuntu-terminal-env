# Ubuntu Env Setup

Run these commands in your terminal to get started.

## Get git branch in terminal (Bash only)

```sh
wget -qO- https://gitlab.com/xxaccexx/ubuntu-terminal-env/-/raw/master/install-git-branch-prompt.sh | bash
```

## Install Node.JS LTS version

```sh
wget -qO- https://gitlab.com/xxaccexx/ubuntu-terminal-env/-/raw/master/install-node-lts.sh | bash
```

## Install preferred optins for VSCode

```sh
wget -qO- https://gitlab.com/xxaccexx/ubuntu-terminal-env/-/raw/master/install-vscode-settings.sh | bash
```
