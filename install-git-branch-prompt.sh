wget https://gitlab.com/xxaccexx/ubuntu-terminal-env/-/raw/master/templates/.bash_config -P ~

echo "" >> .bashrc
echo "# load in the .bash_config file" >> .bashrc
echo "if [ -f ~/.bash_config ]; then" >> .bashrc
echo "  . ~/.bash_config" >> .bashrc
echo "fi" >> .bashrc
